# config.py
# To keep things nicely organized, we use a configuration
# class in a separate Python module.

import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    """Config class which holds various configuration values"""

    # used by various plugins, e.g. flask-wtf, against CSRF attacks
    SECRET_KEY = os.environ.get("SECRET_KEY") or "you-will-never-guess"

    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL") or "sqlite:///" + os.path.join(basedir, "app.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # how many posts per page should we display?
    POSTS_PER_PAGE = 10

    # setting up email
    # if the email server is not set via the environment, emailing will be disabled
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = os.environ.get("MAIL_PORT")
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS") is not None
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    MAIL_SENDER = os.environ.get("MAIL_SENDER", "localhost")
    ADMINS = os.environ.get("ADMINS", "localhost").split(",")
