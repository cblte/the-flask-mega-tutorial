# email.py
#
# email helper function

from threading import Thread

from flask import render_template
from flask_mail import Message

from app import app, mail


# Python has support for running asynchronous tasks, actually in more than one way. The threading and multiprocessing
# modules can both do this. Starting a background thread for email being sent is much less resource intensive than
# starting a brand new process, so I'm going to go with that approach:
def send_async_mail(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, text_html):
    msg = Message(subject=subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = text_html
    app.logger.info(f"Send email from {sender} to {recipients} with subject '{subject}'.")
    # mail.send(msg) The send_async_email function now runs in a background thread, invoked via the Thread class in the
    # last line of send_email(). With this change, the sending of the email will run in the thread, and when the process
    # completes the thread will end and clean itself up.
    Thread(target=send_async_mail, args=(app, msg)).start()


def send_password_reset_email(user):
    token = user.get_reset_password_token()
    send_email(
        "[Microblog] Reset Your Password",
        sender=app.config["MAIL_SENDER"],
        recipients=[user.email],
        text_body=render_template("email/reset_password.txt", user=user, token=token),
        text_html=render_template("email/reset_password.html", user=user, token=token),
    )
