# forms.py
# The Flask-WTF extension uses Python classes to represent web forms.
# A form class simply defines the fields of the form as class variables.
# Once again having separation of concerns in mind,
# a app/forms.py module to store my web form classes will be used.

from ast import Pass

from flask_wtf import FlaskForm
from wtforms import BooleanField, PasswordField, StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Email, EqualTo, ValidationError, Length

from app.models import User


class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember_me = BooleanField("Remember Me")
    submit = SubmitField("Sign In")


class RegistrationForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired(), Length(min=4, max=64)])
    # using stock validator Email() from WTForms (install pip install email-validator to use it)
    email = StringField("Email", validators=[DataRequired(), Email()])
    # user needs to enter his password twice, password2 uses another stock validator to check if both passwords are equal
    password = PasswordField("Password", validators=[DataRequired(), Length(min=8, max=64)])
    password2 = PasswordField("Repeat Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Register")

    # Adding any methods that match the pattern `validate_<field_name>`, WTForms
    # takes those as custom validators and invokes them in addition to the stock
    # validators.
    #
    # Added two of those methods to this class for the `username` and `email`
    # fields. In this case I want to make sure that the username and email
    # address entered by the user are not already in the database, so these two
    # methods issue database queries expecting there will be no results. In the
    # event a result exists, a validation error is triggered by raising an
    # exception of type `ValidationError`. The message included as the argument
    # in the exception will be the message that will be displayed next to the
    # field for the user to see.

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError("Please use a different username.")

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError("Please use a different email address.")


class EditProfileForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired(), Length(min=4, max=64)])
    about_me = TextAreaField("About Me", validators=[Length(min=0, max=140)])
    submit = SubmitField("Save Profile Information")

    def __init__(self, original_username, *args, **kwargs):
        # save the username as instance variable to have it later for checks
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    # Adding any methods that match the pattern `validate_<field_name>`, WTForms
    # takes those as custom validators and invokes them in addition to the stock
    # validators.
    def validate_username(self, username):
        # if the username is not the same, check database
        if self.username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError("Please use a different username.")


class EmptyForm(FlaskForm):
    submit = SubmitField("Submit")


class PostForm(FlaskForm):
    post = TextAreaField("Say Something", validators=[DataRequired(), Length(min=1, max=140)])
    submit = SubmitField("Submit")


class ResetPasswordRequestForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()])
    submit = SubmitField("Submit")


class ResetPasswordForm(FlaskForm):
    password = PasswordField("Password", validators=[DataRequired(), Length(min=8, max=64)])
    password2 = PasswordField("Repeat Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Request Password Reset")
