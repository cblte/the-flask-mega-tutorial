# models.py
# store database models in this file
#
# Every time the database is modified it is necessary to generate
# a database migration by executing:
#   flask db migrate -m "insert comment here"
# when output looks good, run
#   flask db upgrade
# to apply changes


from base64 import b64encode
from datetime import datetime
from email.policy import default
from hashlib import md5
from time import time

import jwt

# import the generic mixin class
from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash

# import LoginManager
from app import app, db, login


@login.user_loader
def load_user(id):
    """load_user asks the User class if a user id is present.
    Needs to convert the string argument into an integer as the database
    only use numeric IDs.
    """
    return User.query.get(int(id))


# Note that I am not declaring this table as a model, like I did for the users
# and posts tables. Since this is an auxiliary table that has no data other than
# the foreign keys, I created it without an associated model class.
followers = db.Table(
    "followers",
    db.Column("follower_id", db.Integer, db.ForeignKey("user.id")),
    db.Column("followed_id", db.Integer, db.ForeignKey("user.id")),
)


class User(UserMixin, db.Model):
    """ "User Table which is holds the users information.
    User is identified by an id, a username, the email address and a hashed password
    """

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    # high level relationship
    # if a user stored in u, the expression u.posts will
    # run a database query that returns all the posts written by that user.
    posts = db.relationship("Post", backref="author", lazy="dynamic")
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    followed = db.relationship(
        "User",
        secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref("followers", lazy="dynamic"),
        lazy="dynamic",
    )

    def __repr__(self):
        return "<User {}>".format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        b64 = b64encode(self.email.encode("utf8"))
        digest = md5(b64).hexdigest()
        return "https://avatar.tobi.sh/{}?size={}".format(digest, size)

    def is_following(self, user):
        # using a filter query to count the amount of id associated
        # if the amount is greater 0 then the user already follows
        return self.followed.filter(followers.c.followed_id == user.id).count() > 0

    def follow(self, user):
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        if self.is_following(user):
            self.followed.remove(user)

    def followed_posts(self):
        # get a joined list of posts from users I follow
        followed_posts = Post.query.join(followers, (followers.c.followed_id == Post.user_id)).filter(
            followers.c.follower_id == self.id
        )
        # get all of my own posts
        own_posts = Post.query.filter_by(user_id=self.id)
        # return the combined list ordered by post date.
        return followed_posts.union(own_posts).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=600):
        """Generate a token with two fields.
        - reset_password: user.id
        - exp : current time + expiration time
        """
        return jwt.encode(
            {"reset_password": self.id, "exp": time() + expires_in}, app.config["SECRET_KEY"], algorithm="HS256"
        )

    @staticmethod
    def verify_reset_password_token(token):
        """
        This method takes a token and attempts to decode it by invoking PyJWT's jwt.decode() function. If the token
        cannot be validated or is expired, an exception will be raised, and in that case we catch it to prevent the
        error, and then return None to the caller. If the token is valid, then the value of the reset_password key
        from the token's payload is the ID of the user, so we can load the user and return it.
        """
        try:
            id = jwt.decode(token, app.config["SECRET_KEY"], algorithms=["HS256"])["reset_password"]
        except:
            return
        return User.query.get(id)


class Post(db.Model):
    """Posts Table which is linked one-to-many to a user. One user writes many posts.
    Posts have an id, a body, a timestamp and user_id field (foreign key).
    """

    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(280))
    # function as a default, SQLAlchemy will set the field to the value of
    # calling that function (did not include the () after utcnow, passing the function itself,
    # and not the result of calling it).
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    def __repr__(self):
        return "<Post #{} - {} - {}>".format(self.id, self.timestamp, self.body)
