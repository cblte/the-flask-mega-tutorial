# errors.py handling errors the correct way
#
# To declare a custom error handler, the @errorhandler decorator is used. I'm
# going to put my error handlers in a new app/errors.py module.



from flask import render_template
from app import app, db


@app.errorhandler(404)
def not_found_error(error):
    return render_template("404.html"), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template("500.html"), 500
