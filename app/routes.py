# routes.py
# this file contains our accessible routes

# The routes are the different URLs that the application implements.
# In Flask, handlers for the application routes are written as
# Python functions, called view functions.
# View functions are mapped to one or more route URLs so that
# Flask knows what logic to execute when a client requests a given URL.

from crypt import methods
from datetime import datetime

from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user
from werkzeug.urls import url_parse

from app import app, db
from app.email import send_password_reset_email
from app.forms import (
    EditProfileForm,
    EmptyForm,
    LoginForm,
    PostForm,
    RegistrationForm,
    ResetPasswordRequestForm,
    ResetPasswordForm,
)
from app.models import Post, User


# The @before_request decorator from Flask register the decorated function to be
# executed right before the view function. This is extremely useful because now
# we can insert code that we want to execute before any view function in the
# application, and we can have it in a single place.
@app.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
@login_required
def index():
    form = PostForm()
    if form.validate_on_submit():
        post = Post(body=form.post.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash("Your post is now live")
        return redirect(url_for("index"))

    page = request.args.get("page", 1, type=int)
    # get a Pagination object (https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination)
    posts = current_user.followed_posts().paginate(page, app.config["POSTS_PER_PAGE"], False)

    next_url = url_for("index", page=posts.next_num) if posts.has_next else None
    prev_url = url_for("index", page=posts.prev_num) if posts.has_prev else None
    return render_template(
        "index.html", title="Home", form=form, posts=posts.items, next_url=next_url, prev_url=prev_url
    )


@app.route("/login", methods=["GET", "POST"])
def login():
    """view for log in.
    Instantiating a `LoginForm` object and simply passing the `form` object created
    in the line above (and shown on the right side) to the template with the
    name `form` (shown on the left).
    """

    # The current_user variable comes from Flask-Login and can be used
    # at any time during the handling to obtain the user object that
    # represents the client of the request. The value of this variable
    # can be a user object from the database (which Flask-Login reads
    # through the user loader callback I provided above), or a special
    # anonymous user object if the user did not log in yet.

    # if the user is already know, redirect to index page
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    # user not know, show login form
    form = LoginForm()

    if form.validate_on_submit():
        # get the first result from the user table filtering by username
        # will return the user object or None if not found
        user = User.query.filter_by(username=form.username.data).first()

        # test if user was found and if found, test if password matches
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("login"))

        # username and password match, register user with a session
        login_user(user, remember=form.remember_me.data)

        # redirect back to where the user came from and was not allowed to view
        # and if that does not exists, redirect back to the index page
        next_page = request.args.get("next")
        if not next_page or url_parse(next_page).netloc != "":
            next_page = url_for("index")

        return redirect(next_page)

    # if not submitted, render login form to the browser
    return render_template("login.html", title="Log In", form=form)


@app.route("/logout")
def logout():
    # log the user out and clean the remember_me cookie
    logout_user()
    return redirect(url_for("index"))


@app.route("/reset_password_request", methods=["GET", "POST"])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)

        flash("If a user with this email exists, we have sent him a password reset email.")
        return redirect(url_for("login"))

    return render_template("reset_password_request.html", title="Reset Password", form=form)


@app.route("/reset_password/<token>", methods=["GET", "POST"])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for("index"))

    # try to get a user object with the passed token
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for("index"))

    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash("Your password has been reset!")
        return redirect(url_for("login"))

    return render_template("reset_password.html", title="Reset Password", user=user, form=form)


@app.route("/register", methods=["GET", "POST"])
def register():
    """display the form were a user can register to access the website"""

    if current_user.is_authenticated:
        return redirect(url_for("index"))

    form = RegistrationForm()

    if form.validate_on_submit():
        # at this point, the model has already validated the form.
        # if something is not right, username or email the form will not be a validated one
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Congratulations, you are now a registered user")
        return redirect(url_for("login"))

    # if not submitted, render register form to the browser
    return render_template("register.html", title="Register", form=form)


@app.route("/user/<username>")
@login_required
def user(username):
    # query for user, if not found, send a 404 error back to the client
    user = User.query.filter_by(username=username).first_or_404()

    page = request.args.get("page", 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(page, app.config["POSTS_PER_PAGE"], False)

    next_url = url_for("user", username=user.username, page=posts.next_num) if posts.has_next else None
    prev_url = url_for("user", username=user.username, page=posts.prev_num) if posts.has_prev else None

    # To render the follow or unfollow button, need to instantiate an
    # EmptyForm object and pass it to the user.html template. Because these two
    # actions are mutually exclusive, pass a single instance of this
    # generic form to the template:
    form = EmptyForm()
    return render_template("user.html", user=user, posts=posts.items, form=form, next_url=next_url, prev_url=prev_url)


@app.route("/edit_profile", methods=["GET", "POST"])
def edit_profile():
    # need to pass current username so we are able to check in the form
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        # if form submitted via post, update database fields from form
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash("Your changes have been saved.")
        return redirect(url_for("edit_profile"))
    elif request.method == "GET":
        # for the initial get request we need to pre populate the form fields
        # as in the case something goes wrong, WTForms fills them for us.
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me

    return render_template("edit_profile.html", title="Edit Profile", form=form)


@app.route("/follow/<username>", methods=["POST"])
@login_required
def follow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        # check if none or current_user
        if user is None:
            flash("User {} not found.".format(username))
            return redirect(url_for("index"))
        if user is current_user:
            flash("You cannot follow yourself")
            return redirect(url_for("user", username=username))
        # all ok, follow user and proceed
        current_user.follow(user)
        db.session.commit()
        flash("You are now following {}.".format(username))
        return redirect(url_for("user", username=username))
    else:
        # if the form submission fails, redirect to index
        return redirect(url_for("index"))


@app.route("/unfollow/<username>", methods=["POST"])
@login_required
def unfollow(username):
    form = EmptyForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=username).first()
        # check if none or current_user
        if user is None:
            flash("User {} not found.".format(username))
            return redirect(url_for("index"))
        if user is current_user:
            flash("You cannot unfollow yourself")
            return redirect(url_for("user", username=username))
        # all ok, follow user and proceed
        current_user.unfollow(user)
        db.session.commit()
        flash("You are not following {} anymore.".format(username))
        return redirect(url_for("user", username=username))
    else:
        # if the form submission fails, redirect to index
        return redirect(url_for("index"))


@app.route("/explore")
@login_required
def explore():
    page = request.args.get("page", 1, type=int)
    # get a Pagination object (https://flask-sqlalchemy.palletsprojects.com/en/2.x/api/#flask_sqlalchemy.Pagination)
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(page, app.config["POSTS_PER_PAGE"], False)
    next_url = url_for("explore", page=posts.next_num) if posts.has_next else None
    prev_url = url_for("explore", page=posts.prev_num) if posts.has_prev else None
    return render_template("index.html", title="Explorer", posts=posts.items, next_url=next_url, prev_url=prev_url)
