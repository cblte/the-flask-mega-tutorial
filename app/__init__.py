# __init__.py
# this gets executed when the parent imports 'app' package

import logging
import os
from logging.handlers import RotatingFileHandler, SMTPHandler

from config import Config
from flask import Flask
from flask_login import LoginManager
from flask_mail import Mail
from flask_migrate import Migrate
from flask_moment import Moment
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config.from_object(Config)

# the LoginManager needs to be initialized right after the application instance
login = LoginManager(app)

# require login for protected views
login.login_view = "login"
# The 'login' value above is the function (or endpoint) name for the login view.
# In other words, the name you would use in a url_for() call to get the URL.

# database object which represents the database
db = SQLAlchemy(app)
# migration engine
migrate = Migrate(app, db)

# handling time and dates
moment = Moment(app)

# flask email object - see README.md > Email Support for more information
mail = Mail(app)


# add logging to email if we are NOT in debug mode
if not app.debug:
    if app.config["MAIL_SERVER"]:
        auth = None
        if app.config["MAIL_USERNAME"] or app.config["MAIL_PASSWORD"]:
            auth = (app.config["MAIL_USERNAME"], app.config["MAIL_PASSWORD"])
        secure = None
        if app.config["MAIL_USE_TLS"]:
            secure = ()
        mail_handler = SMTPHandler(
            mailhost=(app.config["MAIL_SERVER"], app.config["MAIL_PORT"]),
            fromaddr="noreply@" + app.config["MAIL_SERVER"],
            toaddrs=app.config["ADMINS"],
            subject="MICROBLOG FAILURE",
            credentials=auth,
            secure=secure,
        )
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

    # setup a file logger
    if not os.path.exists("logs"):
        os.mkdir("logs")
    # The RotatingFileHandler class is nice because it rotates the logs, ensuring that the log files do not grow too
    # large when the application runs for a long time. In this case limiting the size of the log file to 10KB, and
    # keeping the last ten log files as backup.
    file_handler = RotatingFileHandler("logs/microblog.log", maxBytes=10240, backupCount=10)

    # The logging.Formatter class provides custom formatting for the log messages. Since these messages are going to a
    # file, containing as much information as possible. Using a format that includes the timestamp, the logging level,
    # the message and the source file and line number from where the log entry originated.
    file_handler.setFormatter(logging.Formatter("%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]"))

    # To make the logging more useful, lowering the logging level to the INFO category, both in the application
    # logger and the file logger handler. In case you are not familiar with the logging categories, they are `DEBUG`,
    # `INFO`, `WARNING`, `ERROR` and `CRITICAL` in increasing order of severity.
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)

    app.logger.setLevel(logging.INFO)

    # As a first interesting use of the log file, the server writes a line to the logs each time it starts. When this
    # application runs on a production server, these log entries will tell you when the server was restarted.
    app.logger.info("Microblog startup")


# import later. it is a workaround to circular imports,
# a common problem with Flask applications.
# import models module which defines the structure of the database
from app import errors, models, routes
