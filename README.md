# the-flask-mega-tutorial

<https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world>

## setup

- install python3
- install python3-venv
- create dir `microblog`
- create virtualenv `python -m venv venv`
- create folder `app` for the application and add code files
  or clone this project
- type `export FLASK_APP=microblog.py` into console to let
  flask know about our application
- then type `flask run` to execute
- open `http://localhost:5000` to view app

BUT before running the application, make sure you have some environment variables set

``` bash
export MAIL_SERVER=spitaler.uberspace.de
export MAIL_PORT=587
export MAIL_USE_TLS=1
export MAIL_USERNAME="noreply@zn80.net"
export MAIL_PASSWORD="<mail_username-password>"
export MAIL_SENDER="noreply@zn80.net"
```

## versions

- v0.0: initial setup
- v0.1: chapter 1 - basic flask app
- v0.2: chapter 2 - templates
- v0.3: chapter 3 - install Flask-WTF package
- v0.4: chapter 4 - Database introduction
- v0.5: chapter 5 - Setting up User Logins
- v0.6: chapter 6 - Nice Profile Pages and Avatars
- v0.7: chapter 7 - Better Error handling via E-Mail and Log
- v0.8: chapter 8 - Followers and unit tests
- v0.9: chapter 9 - pagination
- v0.10: chapter 10 - email support

## python-dotenv

Since environment variables aren't remembered across terminal sessions, you may find tedious to always have to set the `FLASK_APP` environment variable when you open a new terminal window.
Starting with version 1.0, Flask allows you to register environment variables that you want to be automatically imported when you run the flask command.
To use this option you have to install the python-dotenv package:

`(venv) $ pip install python-dotenv`

Then you can just write the environment variable name and value in a file named `.flaskenv` located in the top-level directory of the project:

> `.flaskenv`: Environment variables for flask command
>
> `FLASK_APP=microblog.py`

## app/templates

Description of the templates.
What they are for and how they work.

### base.html

The base template which includes css and menu for all other templates.

### index.html

The template for index page aka frontpage aka home.
Lists all blog entries.

### login.html

This template expects a form object instantiated from the LoginForm class to be given as an argument, which you can see referenced as form.
This argument will be sent by the login view function, which I still haven't written.

The HTML `<form>` element is used as a container for the web form.
The `action` attribute of the form is used to tell the browser the URL that should be used when submitting the information the user entered in the form.
When the action is set to an empty string the form is submitted to the URL that is currently in the address bar, which is the URL that rendered the form on the page.
The `method` attribute specifies the HTTP request method that should be used when submitting the form to the server.
The default is to send it with a `GET` request, but in almost all cases, using a `POST` request makes for a better user experience because requests of this type can submit the form data in the body of the request, while GET requests add the form fields to the URL, cluttering the browser address bar.
The `novalidate` attribute is used to tell the web browser to not apply validation to the fields in this form, which effectively leaves this task to the Flask application running in the server.
Using `novalidate` is entirely optional, but for this first form it is important that you set it because this will allow you to test server-side validation later in this chapter.

The `form.hidden_tag()` template argument generates a hidden field that includes a token that is used to protect the form against CSRF attacks.
All you need to do to have the form protected is include this hidden field and have the SECRET_KEY variable defined in the Flask configuration.
If you take care of these two things, Flask-WTF does the rest for you.

Show validator erros by adding for loops right after the username and password fields that render the error messages added by the validators in red color.
As a general rule, any fields that have validators attached will have any error messages that result from validation added under `form.<field_name>.errors`.
This is going to be a list, because fields can have multiple validators attached and more than one may be providing error messages to display to the user.

## The First Database Migration

The First Database Migration

With the migration repository in place, it is time to create the first database migration, which will include the users table that maps to the User database model. There are two ways to create a database migration: manually or automatically. To generate a migration automatically, Alembic compares the database schema as defined by the database models, against the actual database schema currently used in the database. It then populates the migration script with the changes necessary to make the database schema match the application models. In this case, since there is no previous database, the automatic migration will add the entire User model to the migration script. The flask db migrate sub-command generates these automatic migrations:

``` shell
(venv) $ flask db migrate -m "users table"
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.autogenerate.compare] Detected added table 'user'
INFO  [alembic.autogenerate.compare] Detected added index 'ix_user_email' on '['email']'
INFO  [alembic.autogenerate.compare] Detected added index 'ix_user_username' on '['username']'
  Generating /home/miguel/microblog/migrations/versions/e517276bb1c2_users_table.py ... done
```

The output of the command gives you an idea of what Alembic included in the migration. The first two lines are informational and can usually be ignored. It then says that it found a user table and two indexes. Then it tells you where it wrote the migration script. The `e517276bb1c2` code is an automatically generated unique code for the migration (it will be different for you). The comment given with the -m option is optional, it adds a short descriptive text to the migration.

The generated migration script is now part of your project, and needs to be incorporated to source control. You are welcome to inspect the script if you are curious to see how it looks. You will find that it has two functions called `upgrade()` and `downgrade()`. The `upgrade()` function applies the migration, and the `downgrade()` function removes it. This allows Alembic to migrate the database to any point in the history, even to older versions, by using the downgrade path.

The `flask db migrate` command does not make any changes to the database, it just generates the migration script. To apply the changes to the database, the `flask db upgrade` command must be used.

``` shell
(venv) $ flask db upgrade
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade  -> e517276bb1c2, users table
```

Because this application uses SQLite, the `upgrade` command will detect that a database does not exist and will create it (you will notice a file named app.db is added after this command finishes, that is the SQLite database). When working with database servers such as MySQL and PostgreSQL, you have to create the database in the database server before running `upgrade`.

Note that Flask-SQLAlchemy uses a "snake case" naming convention for database tables by default. For the `User` model above, the corresponding table in the database will be named `user`. For a `AddressAndPhone` model class, the table would be named `address_and_phone`. If you prefer to choose your own table names, you can add an attribute named `__tablename__` to the model class, set to the desired name as a string.

## add Posts table

After adding class to `models.py` migrate and upgrade db

``` shell
✦ ❯ flask db migrate -m "posts table" 
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
ERROR [flask_migrate] Error: Target database is not up to date.

the-flask-mega-tutorial on  main [!] via 🐍 v3.10.6 (venv) 
✦ ❯ flask db upgrade                  
INFO  [alembic.runtime.migration] Context impl SQLiteImpl.
INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
INFO  [alembic.runtime.migration] Running upgrade  -> 1aea39cc02aa, users table

the-flask-mega-tutorial on  main [!] via 🐍 v3.10.6 (venv) 
```

## setup flask shell context

The `flask shell` command is another very useful tool in the `flask` umbrella of commands. The `shell` command is the second "core" command implemented by Flask, after `run`. The purpose of this command is to start a Python interpreter in the context of the application.

With a regular interpreter session, the `app` symbol is not known unless it is explicitly imported, but when using `flask shell`, the command pre-imports the application instance. The nice thing about `flask shell` is not that it pre-imports `app`, but that you can configure a "shell context", which is a list of other symbols to pre-import.

``` Python
from app import app, db
from app.models import User, Post

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}

```

The `app.shell_context_processor` decorator registers the function as a shell context function. When the `flask shell` command runs, it will invoke this function and register the items returned by it in the shell session. The reason the function returns a dictionary and not a list is that for each item you have to also provide a name under which it will be referenced in the shell, which is given by the dictionary keys.

After you add the shell context processor function you can work with database entities without having to import them:

``` Shell
(venv) $ flask shell
>>> db
<SQLAlchemy engine=sqlite:////Users/migu7781/Documents/dev/flask/microblog2/app.db>
>>> User
<class 'app.models.User'>
>>> Post
<class 'app.models.Post'>
```

## Flask Login

Introduction to Flask-Login

In this chapter I'm going to introduce you to a very popular Flask extension called [Flask-Login](https://flask-login.readthedocs.io/). This extension manages the user logged-in state, so that for example users can log in to the application and then navigate to different pages while the application "remembers" that the user is logged in. It also provides the "remember me" functionality that allows users to remain logged in even after closing the browser window.

### Preparing The User Model for Flask-Login

The Flask-Login extension works with the application's user model, and expects certain properties and methods to be implemented in it. This approach is nice, because as long as these required items are added to the model, Flask-Login does not have any other requirements, so for example, it can work with user models that are based on any database system.

The four required items are listed below:

- `is_authenticated`: a property that is True if the user has valid credentials or False otherwise.
- `is_active`: a property that is True if the user's account is active or False otherwise.
- `is_anonymous`: a property that is False for regular users, and True for a special, anonymous user.
- `get_id()`: a method that returns a unique identifier for the user as a string (unicode, if using Python 2).

### User Loader Function

Flask-Login keeps track of the logged in user by storing its unique identifier in Flask's user session, a storage space assigned to each user who connects to the application. Each time the logged-in user navigates to a new page, Flask-Login retrieves the ID of the user from the session, and then loads that user into memory.

## Error Handling

After adding the code, there are two approaches to test this feature. The easiest one is to use the SMTP debugging server from Python. This is a fake email server that accepts emails, but instead of sending them, it prints them to the console. To run this server, open a second terminal session and run the following command on it:

`(venv) $ python -m smtpd -n -c DebuggingServer localhost:8025`

Leave the debugging SMTP server running and go back to your first terminal and set export `MAIL_SERVER=localhost` and `MAIL_PORT=8025` in the environment (use `set` instead of `export` if you are using Microsoft Windows). Make sure the FLASK_ENV variable is set to production or not set at all, since the application will not send emails in debug mode. Run the application and trigger the SQLAlchemy error one more time to see how the terminal session running the fake email server shows an email with the full stack trace of the error.

Yet another alternative is to use a dedicated email service such as [SendGrid](https://sendgrid.com/), which allows you to send up to 100 emails per day on a free account. The SendGrid blog has a detailed tutorial on [using the Twilio SendGrid service in a Flask application](https://sendgrid.com/blog/sending-emails-from-python-flask-applications-with-twilio-sendgrid/).

## Followers

This is a bit more complicated as expected because of the query wee need to create.

Create a many-to-many relationship from user to users. This is done with an auxiliary table called an _association table_.

One user can follow many users and a user has many followers. Because of this, we just have users and our table only needs `follower_id` and `followed_id`. Since the `followers` table is an auxiliary table that has no data other than the foreign keys, I created it without an associated model class.

The user class gets a `followed` field that represents a relationship with between two users.

## Pagenation and PostForm

So, why the redirect? It is a standard practice to respond to a POST request generated by a web form submission with a redirect. This helps mitigate an annoyance with how the refresh command is implemented in web browsers. All the web browser does when you hit the refresh key is to re-issue the last request. If a POST request with a form submission returns a regular response, then a refresh will re-submit the form. Because this is unexpected, the browser is going to ask the user to confirm the duplicate submission, but most users will not understand what the browser is asking them. But if a POST request is answered with a redirect, the browser is now instructed to send a GET request to grab the page indicated in the redirect, so now the last request is not a POST request anymore, and the refresh command works in a more predictable way.

This simple trick is called the [Post/Redirect/Get](https://en.wikipedia.org/wiki/Post/Redirect/Get) pattern. It avoids inserting duplicate posts when a user inadvertently refreshes the page after submitting a web form.

## Email Support

Installing `flask-email` and python json web tokens `pyjwt` for creating secure tokens for password reset. Mail is configured in the `app.config`. The already in place configuration variables are modeld after the flask email requirements.

If you want to use an emulated email server, Python provides one that is very handy that you can start in a second terminal with the following command:

`(venv) $ python -m smtpd -n -c DebuggingServer localhost:8025`
To configure for this server you will need to set two environment variables:

``` bash
export MAIL_SERVER=localhost
export MAIL_PORT=8025
```

If you prefer to have emails sent for real, you need to use a real email server. If you have one, then you just need to set the MAIL_SERVER, MAIL_PORT, MAIL_USE_TLS, MAIL_USERNAME and MAIL_PASSWORD environment variables for it. If you want a quick solution, you can use a Gmail account to send email, with the following settings:

``` bash
export MAIL_SERVER=smtp.googlemail.com
export MAIL_PORT=587
export MAIL_USE_TLS=1
export MAIL_USERNAME=<your-gmail-username>
export MAIL_PASSWORD=<your-gmail-password>
export ADMINS="admin_one@example.com,admin_two@example.com"
```

For Uberspace this looks similar to the next block, do not forget to update the server address to your own

``` bash
export MAIL_SERVER=spitaler.uberspace.de
export MAIL_PORT=587
export MAIL_USE_TLS=1
export MAIL_USERNAME=noreply@spitaler.uberspace.de
export MAIL_PASSWORD=<your-email-account-password>
export ADMINS="admin_one@example.com,admin_two@example.com"
```



The ADMINS is a comma separated list, without spaces!

If you are using Microsoft Windows, you need to replace export with set in each of the export statements above.

If you'd like to use a real email server, but don't want to complicate yourself with the Gmail configuration, SendGrid is a good option that gives you 100 emails per day using a free account.
